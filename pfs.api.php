<?php

use Drupal\Core\Access\AccessResult;

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Control access to a private file.
 *
 * Modules may implement this hook if they want to have a say in whether or not
 * a given URI can be accessed.
 *
 * @param string $uri
 *   The file URI; for instance, 'private://foo-bar/index.html'.
 *
 * @return \Drupal\Core\Access\AccessResultInterface
 *   The access result. If all implementations of this hook return
 *   AccessResultInterface objects whose value is isAllowed(), access would be
 *   granted. If isForbidden(), access denied.
 *
 * @see pfs_file_download()
 *
 * @ingroup pfs_api
 */
function hook_pfs_access(string $uri) {
  // Example code that would allow authenticated users to access all files under
  // foo-bar directory.
  if (strpos($uri, '//foo-bar/') !== FALSE) {
    if (\Drupal::currentUser()->isAuthenticated()) {
      return AccessResult::allowed();
    }
    else {
      return AccessResult::forbidden();
    }
  }
}

/**
 * @} End of "addtogroup hooks".
 */
